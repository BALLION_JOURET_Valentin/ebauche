 module maison(n,taille,longeur,largeur){ 
     if(n>0){
         difference(){
              e=rands(10,taille,1)[0];
            cube([longeur,largeur,taille]);
             /*if(porte!=0){
                cube([1.5,5,taille/2]);
                porte=0;
              }*/
             translate([0.5,0.5,1])cube([longeur-1,largeur-1,taille]);
             translate([e,-0.1,e/2])cube([longeur/6,1,taille/3]);
             for(a=[0:1]){//face X
                c= rands(e+longeur/6,longeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+longeur/6 < longeur-5){
                        translate([c,-0.1,e/2])cube([longeur/6,1,taille/3]);
                    }
                }
                 for(a=[0:1]){//face Y
                c= rands(largeur/6,largeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+largeur/6 < largeur-5){
                        translate([longeur-1,c,e/2])cube([1.1,largeur/6,taille/3]);
                    }
                }
                 for(a=[0:1]){//face X 2
                c= rands(longeur/6,longeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+longeur/6 < longeur-5){
                        translate([c,largeur-1,e/2])cube([longeur/6,1.1,taille/3]);
                    }
                }
                 for(a=[0:1]){//face Y 2 
                c= rands(largeur/6,largeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+largeur/6 < largeur-5){
                        translate([-0.1,c,e/2])cube([1.1,largeur/6,taille/3]);
                    }
                }
                }
             translate([0,0,taille])maison(n-1,taille,longeur,largeur);
    }else{
     e=round(rands(3,5,2)[0]);
        
    //toiture
    TrianglePoints= [ [-e,-e,0],[longeur+e,-e,0],[longeur+e,largeur+e,0],[-e,largeur+e,0],[longeur/2,largeur/2,taille]];
    TriangleFace  = [  [0,1,2,3],[0,1,4],[1,2,4],[2,3,4],[3,0,4]];
    polyhedron(TrianglePoints, TriangleFace);
    cube([longeur/6,largeur/6,taille/1.5]);

    }
}
module build(a,maxi){
    etage = round(rands(1,2,1)[0]);
    taille= rands(5,14,2)[0];
    longeur = rands(10,30,5)[0];
    largeur = rands(10,25,2)[0];
    if(a<2){
        translate([sin(360*a/maxi)*(120*0.75),cos(360*a/maxi)*(120*0.75),]) maison(etage,taille,longeur,largeur);
 
    }else if(a<6){
           translate([sin(360*a/maxi)*(100*1.20),cos(360*a/maxi)*(100*1.20),]) maison(etage,taille,longeur,largeur);

    }else if(a<9){
               translate([sin(360*a/maxi)*(100*1.50),cos(360*a/maxi)*(100*1.50),]) maison(etage,taille,longeur,largeur);

    }else if(a<12){
                    translate([sin(360*a/maxi)*(120*1.75),cos(360*a/maxi)*(75*1.75),]) maison(etage,taille,longeur,largeur);
    
    }else{
           translate([sin(360*a/maxi)*(75*2),cos(360*a/maxi)*(100*2),]) maison(etage,taille,longeur,largeur);
    }

}
module pommier(taille, abr, n,pommier) {   
        if (n > 0) {
            // tronc
            cylinder(r1=taille/10, r2=taille/12, h=taille, $fn=20);
            // branches
           z_ =rands(-120,170,30)[0];
            translate([0,0,taille])
                   for(bd = abr) {
                    angx = bd[0]; //anglex
                    angz = bd[1]*z_;
                    coef = bd[2];
                    rotate([angx,0,angz]) pommier(coef*taille, abr, n-1,pommier);
                }
        }
        else{ // feuilles
            pomme = rands(0,10,1)[0];
            if(pommier==1 && pomme<2){
            color("red") translate([0,0,taille/2]) rotate([90,0,0]) sphere(1.5);
            }else{
            color("green")
            translate([0,0,taille/4]) rotate([90,0,0]) sphere(2);
        }
    }
}
module pin(taille){
    translate([0,0,taille])color("green")cylinder(h=taille, r1=taille/5, r2=0, center=true);
    color("BurlyWood") cylinder(h=taille,r1=taille/20,r2=taille/20);
}
    
module tour(){
    cylinder(h=32,r=4,r=4);
    translate([0,0,32])cylinder(h=10,r=5.4,r2=0.1);
}
   

module enceinte(){
    enceinte=round(rands(1,4,1)[0]);
    enceinte1=[[0,-5],[20,-30],[40,-30],[60,-10],[60,20],[40,40],[20,40],[0,15]];
    enceinte2=[[0,-5],[20,-20],[40,-5],[40,15],[20,30],[0,15]];
    enceinte3=[[0,-5],[20,-25],[50,5],[20,35],[0,15]];
    enceinte4=[[0,0],[40,-40],[70,0],[40,40]];
    path1=[[0,1,2,3,4,5,6,7]];
    path2=[[0,1,2,3,4,5]];
    path3=[[0,1,2,3,4]];
    path4=[[0,1,2,3]];
    // echo(enceinte); 
    if(enceinte == 1){
       difference(){
            union(){
                linear_extrude(height = 30)polygon(enceinte1,path1,10);
                translate([-3,-3.5,0])cube([5,15,15]);
            }
            translate([0,0,1])linear_extrude(height = 30)polygon([[1,-2],[20,-28],[40,-28],[58,-10],[58,18],[40,38],[20,38],[1,12]],path1,10);
            translate([-3.1,5,0])rotate([0,90,0])cylinder(h=5.1,r=3,r=3);
    }
        for(i=[0:7]){
        translate(enceinte1[i])tour();
        }
    }else if(enceinte == 2){
        difference(){
            union(){
                linear_extrude(height = 30)polygon(enceinte2,path2,10);
                translate([-3,-2.5,0])cube([5,15,20]);
            }
            translate([0,0,1])linear_extrude(height = 30)polygon([[1,-2.5],[20,-18],[38,-5],[38,15],[20,28],[1,12.5]],path2,10);
            translate([-3.1,5,0])rotate([0,90,0])cylinder(h=6,r=4,r=4);
        }
        for(i=[0:5]){
        translate(enceinte2[i])tour();
        }
    }else if(enceinte == 3){
        difference(){
            union(){
                linear_extrude(height=30) polygon(enceinte3,path3,10);
                translate([-3,-2.5,0])cube([5,20,15]);
            }
            translate([0,0,1])linear_extrude(height = 30)polygon([[2,-5],[20,-22.5],[48,5],[20,32.5],[2,15]],path3,10);
            translate([-3.1,5,0])rotate([0,90,0])cylinder(h=6,r=5,r=5);
        }
        for(i=[0:4]){
        translate(enceinte3[i])tour();
        }
            
    }else if(enceinte == 4){
        difference(){
           union(){ 
                linear_extrude(height = 30)polygon(enceinte4,path4,10);
                translate([-3,-10,0])cube([15,20,20]);
           }
           translate([0,0,1])linear_extrude(height = 30)polygon([[2,0],[40,-38],[68,0],[40,38],[10,18],[2,10]],path4,10);
           translate([-3.1,0,0])rotate([0,90,0])cylinder(h=10,r=5,r=5);
       }
       for(i=[1:3]){
            translate(enceinte4[i])tour();
       }
    }
}        

abr = [ [20,  80, 0.8], // [ angle branche, angle autour du tronc, coef taille]
            [40,    0, 0.6], 
            [30, 125, 0.6],
            [60, -170, 0.6] ];
     

color("green") cube([600,600,0.1],true); //sol
 
for( a = [0:50]){
    pommier= round(rands(0,1,1)[0]); //rands pour la création d'un pommier ou non
    echo(pommier);
       //i= [ rands(-600,sin(360*i/2),20)[0], rands(-600,cos(380*i/2),20)[0]];
  
    i = [  rands(-300,-10,80)[0],  rands(-300,100,50)[0],  0];
    e = [  rands(-300,-10,80)[0],  rands(-300,100,50)[0],  0];
    // vecteur aléatoire pour le placemmente des arbes
    taille_rnd = rands(4,10,2)[0]; // taille de l'abre aléatoire
    taille_pin= rands(10,20,2)[0];
    translate(i) pommier(taille_rnd, abr,4, pommier) ;
    translate(e) pin(taille_pin);
  
}

 
a=0;
x=0;
y=0;

color("green") cube([700,600,0.1],true); //sol
loop = round(rands(4,17,1)[0]);
 for( i =[0:loop]){
     translate([180,-120,0])build(i,loop+1);
    /*etage = round(rands(1,4,1)[0]);
    taille= rands(10,20,2)[0];
    longeur = rands(25,45,5)[0];
    largeur = rands(20,40,2)[0];
    echo(a);
    translate([sin(360*i/6)*(i*35),cos(360*i/6)*(35*i),0]) maison(etage,taille,longeur,largeur);
   */
}
translate([150,200,0.1])rotate([0,0,90])enceinte();

/*module enceinte(longeur,taille,vecteur,n){
          if(n>0){
          //tours
           cylinder(taille,r1=10,r2=10);
          }
          else{
              difference(){
                cube([x+longeur,y,taille/2]);
                translate([1,1,0])cube([x+longeur-2,y-2,taille/2]);
              }
          }
      }
     
 
module enceinte(longeur,taille,vecteur,n){
          if(n>0){
          //tours
            color("grey")cylinder(taille,r1=10,r2=10);
            translate([0,0,taille])color("red")cylinder(taille/2,r1=15,r2=1);
          }
          else{
              difference(){
                color("grey")cube([x+longeur,y,taille/2]);
                translate([1,1,0.1])cube([x+longeur-2,y-2,taille/2]);
              }
              difference(){
                  color("grey")translate([x+longeur/2,-5,15]) cube([longeur/3,longeur/5,30],true);
                  translate([x+longeur/2,longeur/3.5,0])rotate([90,0,0])color("red")cylinder(h=longeur/2,r1=6,r2=6);
              }
          }
      }
     

    
    
  longeur = round(rands(50,200,50)[0]);
  x = round(rands(0,80,5)[0]);
  y = round(rands(80,130,5)[0]);
  for(i = [0:4]){
     hauteur=30;
     enceinte_coor=[[0,0,0],[x+longeur,0,0],[x+longeur,y,0],[0,y,0]];
     translate(enceinte_coor[i]) enceinte(longeur,hauteur,y,4-i);
     echo(enceinte_coor[i]);
  }
  
*/ 
 