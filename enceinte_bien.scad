

module tour(){
    cylinder(h=32,r=4,r=4);
    translate([0,0,32])cylinder(h=10,r=5.4,r2=0.1);
}


    
enceinte=round(rands(1,4,1)[0]);
enceinte1=[[0,-5],[20,-30],[40,-30],[60,-10],[60,20],[40,40],[20,40],[0,15]];
enceinte2=[[0,-5],[20,-20],[40,-5],[40,15],[20,30],[0,15]];
enceinte3=[[0,-5],[20,-25],[50,5],[20,35],[0,15]];
enceinte4=[[0,0],[40,-40],[70,0],[40,40]];
path1=[[0,1,2,3,4,5,6,7]];
path2=[[0,1,2,3,4,5]];
path3=[[0,1,2,3,4]];
path4=[[0,1,2,3]];
// echo(enceinte); 
enceinte=4;
if(enceinte == 1){
   difference(){
        union(){
            linear_extrude(height = 30)polygon(enceinte1,path1,10);
            translate([-3,-3.5,0])cube([5,15,15]);
        }
        translate([0,0,1])linear_extrude(height = 30)polygon([[1,-2],[20,-28],[40,-28],[58,-10],[58,18],[40,38],[20,38],[1,12]],path1,10);
        translate([-3.1,5,0])rotate([0,90,0])cylinder(h=5.1,r=3,r=3);
}
    for(i=[0:7]){
    translate(enceinte1[i])tour();
    }
}else if(enceinte == 2){
    difference(){
        union(){
            linear_extrude(height = 30)polygon(enceinte2,path2,10);
            translate([-3,-2.5,0])cube([5,15,20]);
        }
        translate([0,0,1])linear_extrude(height = 30)polygon([[1,-2.5],[20,-18],[38,-5],[38,15],[20,28],[1,12.5]],path2,10);
        translate([-3.1,5,0])rotate([0,90,0])cylinder(h=6,r=4,r=4);
    }
    for(i=[0:5]){
    translate(enceinte2[i])tour();
    }
}else if(enceinte == 3){
    difference(){
        union(){
            linear_extrude(height=30) polygon(enceinte3,path3,10);
            translate([-3,-2.5,0])cube([5,20,15]);
        }
        translate([0,0,1])linear_extrude(height = 30)polygon([[2,-5],[20,-22.5],[48,5],[20,32.5],[2,15]],path3,10);
        translate([-3.1,5,0])rotate([0,90,0])cylinder(h=6,r=5,r=5);
    }
    for(i=[0:4]){
    translate(enceinte3[i])tour();
    }
        
}else if(enceinte == 4){
    difference(){
       union(){ 
            linear_extrude(height = 30)polygon(enceinte4,path4,10);
            translate([-3,-10,0])cube([15,20,20]);
       }
       translate([0,0,1])linear_extrude(height = 30)polygon([[2,0],[40,-38],[68,0],[40,38],[10,18],[2,10]],path4,10);
       translate([-3.1,0,0])rotate([0,90,0])cylinder(h=10,r=5,r=5);
   }
   for(i=[1:3]){
        translate(enceinte4[i])tour();
   }
}
                       
     
 /*triangle_points =[[0,0],[100,0],[0,100],[10,10],[80,10],[10,80]];
 triangle_paths =[[0,1,2],[3,4,5]];
 polygon(triangle_points,triangle_paths,10)*/