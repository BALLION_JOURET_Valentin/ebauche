 
 //polygon(points=[[0,0],[100,0],[0,100],[10,10],[80,10],[10,80]], paths=[[0,1,2],[3,4,5]],convexity=10);
 
 module maison(n,taille,longeur,largeur){ 
     if(n>0){
         difference(){
              e=rands(10,taille,1)[0];
            cube([longeur,largeur,taille]);
             /*if(porte!=0){
                cube([1.5,5,taille/2]);
                porte=0;
              }*/
             translate([0.5,0.5,1])cube([longeur-1,largeur-1,taille]);
             translate([e,-0.1,e/2])cube([longeur/6,1,taille/3]);
             for(a=[0:1]){//face X
                c= rands(e+longeur/6,longeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+longeur/6 < longeur-5){
                        translate([c,-0.1,e/2])cube([longeur/6,1,taille/3]);
                    }
                }
                 for(a=[0:1]){//face Y
                c= rands(largeur/6,largeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+largeur/6 < largeur-5){
                        translate([longeur-1,c,e/2])cube([1.1,largeur/6,taille/3]);
                    }
                }
                 for(a=[0:1]){//face X 2
                c= rands(longeur/6,longeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+longeur/6 < longeur-5){
                        translate([c,largeur-1,e/2])cube([longeur/6,1.1,taille/3]);
                    }
                }
                 for(a=[0:1]){//face Y 2 
                c= rands(largeur/6,largeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+largeur/6 < largeur-5){
                        translate([-0.1,c,e/2])cube([1.1,largeur/6,taille/3]);
                    }
                }
                }
             translate([0,0,taille])maison(n-1,taille,longeur,largeur);
    }else{
     e=round(rands(3,5,2)[0]);
        
    //toiture
    TrianglePoints= [ [-e,-e,0],[longeur+e,-e,0],[longeur+e,largeur+e,0],[-e,largeur+e,0],[longeur/2,largeur/2,taille]];
    TriangleFace  = [  [0,1,2,3],[0,1,4],[1,2,4],[2,3,4],[3,0,4]];
    polyhedron(TrianglePoints, TriangleFace);
    cube([longeur/6,largeur/6,taille/1.5]);

    }
}
module build(a,maxi){
    etage = round(rands(1,3,1)[0]);
    taille= rands(10,20,2)[0];
    longeur = rands(25,45,5)[0];
    largeur = rands(20,40,2)[0];
    if(a<2){
        translate([sin(360*a/maxi)*(120*0.75),cos(360*a/maxi)*(120*0.75),]) maison(etage,taille,longeur,largeur);
 
    }else if(a<6){
           translate([sin(360*a/maxi)*(100*1.20),cos(360*a/maxi)*(100*1.20),]) maison(etage,taille,longeur,largeur);

    }else if(a<9){
               translate([sin(360*a/maxi)*(100*1.50),cos(360*a/maxi)*(100*1.50),]) maison(etage,taille,longeur,largeur);

    }else if(a<12){
                    translate([sin(360*a/maxi)*(120*1.75),cos(360*a/maxi)*(75*1.75),]) maison(etage,taille,longeur,largeur);
    
    }else{
           translate([sin(360*a/maxi)*(75*2),cos(360*a/maxi)*(100*2),]) maison(etage,taille,longeur,largeur);
    }

}
 
a=0;
//loop=16;
x=0;
y=0;
/*for(i=[0:10]){
    x=rands(0,200,50)[0];
    y=rands(0,200,50)[0];
    coord= concat(coord,x,y);
     echo(coord);
}*/
color("green") cube([600,600,0.1],true); //sol
loop = round(rands(4,17,1)[0]);
 for( i =[0:loop]){
     translate([120,120,0])build(i,loop+1);
    /*etage = round(rands(1,4,1)[0]);
    taille= rands(10,20,2)[0];
    longeur = rands(25,45,5)[0];
    largeur = rands(20,40,2)[0];
    echo(a);
    translate([sin(360*i/6)*(i*35),cos(360*i/6)*(35*i),0]) maison(etage,taille,longeur,largeur);
   */
}
