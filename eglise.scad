cube([15,30,10]);
translate([15/2,0,15])rotate([-90,-90,0])cylinder(h=30,r1=10,r2=10,$fn=3);
translate([0,30,0])cube([15,10,30]);
TrianglePoints= [ [-e,-e,0],[longeur+e,-e,0],[longeur+e,largeur+e,0],[-e,largeur+e,0],[longeur/2,largeur/2,taille]];
    TriangleFace  = [  [0,1,2,3],[0,1,4],[1,2,4],[2,3,4],[3,0,4]];
    polyhedron(TrianglePoints, TriangleFace);