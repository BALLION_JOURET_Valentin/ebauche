 module maison(n,taille,longeur,largeur){ // fonction de création des maisons 
     if(n>0){
         difference(){
              e=rands(10,taille,1)[0];
            cube([longeur,largeur,taille]);
             /*if(porte!=0){
                cube([1.5,5,taille/2]);
                porte=0;
              }*/
             translate([0.5,0.5,1])cube([longeur-1,largeur-1,taille]);
             translate([e,-0.1,e/2])cube([longeur/6,1,taille/3]);
             
             /* Ici je crée des fenètres sur chaque face de la maison */
             for(a=[0:1]){//face X
                c= rands(e+longeur/6,longeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+longeur/6 < longeur-5){
                        translate([c,-0.1,e/2])cube([longeur/6,1,taille/3]);
                    }
                }
                 for(a=[0:1]){//face Y
                c= rands(largeur/6,largeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+largeur/6 < largeur-5){
                        translate([longeur-1,c,e/2])cube([1.1,largeur/6,taille/3]);
                    }
                }
                 for(a=[0:1]){//face X 2
                c= rands(longeur/6,longeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+longeur/6 < longeur-5){
                        translate([c,largeur-1,e/2])cube([longeur/6,1.1,taille/3]);
                    }
                }
                 for(a=[0:1]){//face Y 2 
                c= rands(largeur/6,largeur,1)[0];
                //echo(e);
                //echo(c);
                    if( c+largeur/6 < largeur-5){
                        translate([-0.1,c,e/2])cube([1.1,largeur/6,taille/3]);
                    }
                }
                }
             translate([0,0,taille])maison(n-1,taille,longeur,largeur);
    }else{
     e=round(rands(3,5,2)[0]);
        
    //toiture
    TrianglePoints= [ [-e,-e,0],[longeur+e,-e,0],[longeur+e,largeur+e,0],[-e,largeur+e,0],[longeur/2,largeur/2,taille]];
    TriangleFace  = [  [0,1,2,3],[0,1,4],[1,2,4],[2,3,4],[3,0,4]];
    polyhedron(TrianglePoints, TriangleFace);
    cube([longeur/6,largeur/6,taille/1.5]);

    }
}
module build(a,maxi){ // Fonction qui construit le village qui prendra en paramètre le nombre de maison maximum 
    etage = round(rands(1,2,1)[0]); // Variable aléatoire pour indiquer le nombre d'étage d'un maison
    taille= rands(5,14,2)[0]; // La taille de cette maison
    longeur = rands(10,30,5)[0]; //Longeur 
    largeur = rands(10,25,2)[0]; //largeur
    /* ici je décide de crée mon village en forme de cercle, grâce a la fonction translate, sin et cos, pour au final générer un village grâce a une spirale 
    
    Ainsi cela me permet d'avoir répartitions des maisons plus naturelle . */
    
    if(a<2){ 
        translate([sin(360*a/maxi)*(120*0.75),cos(360*a/maxi)*(120*0.75),]) maison(etage,taille,longeur,largeur);
 
    }else if(a<6){
           translate([sin(360*a/maxi)*(100*1.20),cos(360*a/maxi)*(100*1.20),]) maison(etage,taille,longeur,largeur);

    }else if(a<9){
               translate([sin(360*a/maxi)*(100*1.50),cos(360*a/maxi)*(100*1.50),]) maison(etage,taille,longeur,largeur);

    }else if(a<12){
                    translate([sin(360*a/maxi)*(120*1.75),cos(360*a/maxi)*(75*1.75),]) maison(etage,taille,longeur,largeur);
    
    }else{
           translate([sin(360*a/maxi)*(75*2),cos(360*a/maxi)*(100*2),]) maison(etage,taille,longeur,largeur);
    }

}
module pommier(taille, abr, n,pommier) {   // fonction de création de pommier prenant en paramètre, une taille , la matrice de base pour la création d'un arbre, le nombre de récursion, et si l'abre est un pommier ou non 
        if (n > 0) {
            // tronc
            cylinder(r1=taille/10, r2=taille/12, h=taille, $fn=20);
            // branches
           z_ =rands(-120,170,30)[0];
            translate([0,0,taille])
                   for(bd = abr) {
                    angx = bd[0]; //anglex
                    angz = bd[1]*z_;
                    coef = bd[2];
                    rotate([angx,0,angz]) pommier(coef*taille, abr, n-1,pommier); 
                       
                       /* Ici je crée des troncs de plus en plus petit a chaque niveau de récursion, que je fais tourner autour d'un axe.  Une fois le niveau de récursion atteint je crée des boules de différentes couleurs si c'est une pomme ou non*/
                }
        }
        else{ // feuilles
            pomme = rands(0,10,1)[0];
            if(pommier==1 && pomme<2){
            color("red") translate([0,0,taille/2]) rotate([90,0,0]) sphere(1.5);
            }else{
            color("green")
            translate([0,0,taille/4]) rotate([90,0,0]) sphere(2);
        }
    }
}
module pin(taille){ //Fonction de création de pin qui prendra en paramètre une taille
    translate([0,0,taille])color("green")cylinder(h=taille, r1=taille/5, r2=0, center=true);//Création du cône qui représentant les épines du pin 
    color("BurlyWood") cylinder(h=taille,r1=taille/20,r2=taille/20); // Création du tronc 
}
    
module tour(){ // Fonction de création des tours de l'enceinte
    cylinder(h=32,r=4,r=4); 
    translate([0,0,32])cylinder(h=10,r=5.4,r2=0.1); // Toit 
}
   

module enceinte(){ // Fonction de création de l'enceinte 
    enceinte=round(rands(1,4,1)[0]); // Génération d'un nombre aléatoire pour sélectionner une enceinte parmis les profils créer 
    enceinte1=[[0,-5],[20,-30],[40,-30],[60,-10],[60,20],[40,40],[20,40],[0,15]];
    enceinte2=[[0,-5],[20,-20],[40,-5],[40,15],[20,30],[0,15]];
    enceinte3=[[0,-5],[20,-25],[50,5],[20,35],[0,15]];
    enceinte4=[[0,0],[40,-40],[70,0],[40,40]];
    /* 4 Profils d'enceinte différentes ayant des tailles fixes et un nombre de coté fixe  */ 
    path1=[[0,1,2,3,4,5,6,7]]; 
    path2=[[0,1,2,3,4,5]];
    path3=[[0,1,2,3,4]];
    path4=[[0,1,2,3]];
    //Création des chemins entre les points crée pour crée un polygone 
    // echo(enceinte); 
    if(enceinte == 1){
       difference(){
           // Union de mon polygon qui représente mes murs, et un cube qui fera office de porte 
            union(){ 
                linear_extrude(height = 30)polygon(enceinte1,path1,10);//Création du polygon ayant pour paramètre les coordonnées des points, et le chemin pour la création du polygone , linear_extrude me permet de projeter le polygone en 3D
                translate([-3,-3.5,0])cube([5,15,15]);
            }
            translate([0,0,1])linear_extrude(height = 30)polygon([[1,-2],[20,-28],[40,-28],[58,-10],[58,18],[40,38],[20,38],[1,12]],path1,10);
            // Je soustrais le meme polygone légèrement plus petit, pour faire apparaitre une enceinte 
            translate([-3.1,5,0])rotate([0,90,0])cylinder(h=5.1,r=3,r=3);
    }
        for(i=[0:7]){
        translate(enceinte1[i])tour(); // Je fais appel a ma fonction de création des tours, sur les coordonnées des points de mon polygone
        }
    }else if(enceinte == 2){
        difference(){
            union(){
                linear_extrude(height = 30)polygon(enceinte2,path2,10);
                translate([-3,-2.5,0])cube([5,15,20]);
            }
            translate([0,0,1])linear_extrude(height = 30)polygon([[1,-2.5],[20,-18],[38,-5],[38,15],[20,28],[1,12.5]],path2,10);
            translate([-3.1,5,0])rotate([0,90,0])cylinder(h=6,r=4,r=4);
        }
        for(i=[0:5]){
        translate(enceinte2[i])tour();
        }
    }else if(enceinte == 3){
        difference(){
            union(){
                linear_extrude(height=30) polygon(enceinte3,path3,10);
                translate([-3,-2.5,0])cube([5,20,15]);
            }
            translate([0,0,1])linear_extrude(height = 30)polygon([[2,-5],[20,-22.5],[48,5],[20,32.5],[2,15]],path3,10);
            translate([-3.1,5,0])rotate([0,90,0])cylinder(h=6,r=5,r=5);
        }
        for(i=[0:4]){
        translate(enceinte3[i])tour();
        }
            
    }else if(enceinte == 4){
        difference(){
           union(){ 
                linear_extrude(height = 30)polygon(enceinte4,path4,10);
                translate([-3,-10,0])cube([15,20,20]);
           }
           translate([0,0,1])linear_extrude(height = 30)polygon([[2,0],[40,-38],[68,0],[40,38],[10,18],[2,10]],path4,10);
           translate([-3.1,0,0])rotate([0,90,0])cylinder(h=10,r=5,r=5);
       }
       for(i=[1:3]){
            translate(enceinte4[i])tour();
       }
    }
}        

abr = [ [20,  80, 0.8], // [ angle branche, angle autour du tronc, coef taille]
            [40,    0, 0.6], 
            [30, 125, 0.6],
            [60, -170, 0.6] ];
 
//cette matrice me permet d'avoir des données sur lequels je construit mes arbres 

color("green") cube([600,600,0.1],true); //sol
 
for( a = [0:50]){
    pommier= round(rands(0,1,1)[0]); //rands pour la création d'un pommier ou non
    echo(pommier);
       //i= [ rands(-600,sin(360*i/2),20)[0], rands(-600,cos(380*i/2),20)[0]];
  
    i = [  rands(-300,-10,80)[0],  rands(-300,100,50)[0],  0]; //coordonnée aléatoire des pommiers
    e = [  rands(-300,-10,80)[0],  rands(-300,100,50)[0],  0]; // Coordonnée aléatoire des pins
    // vecteur aléatoire pour le placemmente des arbes
    taille_rnd = rands(4,10,2)[0]; // taille de l'abre aléatoire
    taille_pin= rands(10,20,2)[0]; // Taille d'un pin 
    translate(i) pommier(taille_rnd, abr,4, pommier) ; // Appel a mes fonctions pour construire une foret entière  une zone prédéfinie 
    translate(e) pin(taille_pin);
  
}

 
color("green") cube([700,600,0.1],true); //sol
loop = round(rands(4,17,1)[0]);//Génération d'un nombre aléatoire permettant de me crée un village de taille "aléatoire"
 for( i =[0:loop]){
     translate([180,-120,0])build(i,loop+1); // Appel a la fonction build
    
}
translate([150,-150,0.1])rotate([0,0,90])enceinte(); //Appel a la fonction enceinte

 